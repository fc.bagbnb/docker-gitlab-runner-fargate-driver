ARG VER=1.15

FROM golang:$VER-alpine

RUN apk add --no-cache make git gcc libc-dev curl
